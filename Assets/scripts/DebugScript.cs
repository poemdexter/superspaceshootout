﻿using UnityEngine;
using System.Collections;

public class DebugScript : MonoBehaviour
{
    private UILabel label;

    private void Start()
    {
        label = GetComponent<UILabel>();
    }

    private void Update()
    {
        label.text = "cam " + Camera.main.orthographicSize;
    }
}