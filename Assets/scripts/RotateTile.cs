﻿using UnityEngine;
using System.Collections;

public class RotateTile : MonoBehaviour {

    public float speed;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Time.deltaTime * speed * new Vector3(0f,1f,0f));
	}
}
