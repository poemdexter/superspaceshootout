﻿using UnityEngine;
using System.Collections;

public class Jetpack : MonoBehaviour
{
    public ParticleSystem smoke;
    public float spinSpeed;
    public float adjustPower;
    private float currentSpeed;
    public bool CanSpin { get; set; }
    private bool pushing;
    private bool pushDir; // up is true

    private void Start()
    {
        CanSpin = true;
        currentSpeed = spinSpeed;
    }

    private void Update()
    {
        if (CompareTag("P1"))
        {
            if (Input.GetButtonDown("P1_Up"))
            {
                pushing = true;
                pushDir = true;
            }
            else if (Input.GetButtonDown("P1_Down"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P1_Left"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P1_Right"))
            {
                pushing = true;
                pushDir = true;
            }

            if (Input.GetButtonUp("P1_Up"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P1_Down"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P1_Left"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P1_Right"))
            {
                pushing = false;
            }
        }
        if (CompareTag("P2"))
        {
            if (Input.GetButtonDown("P2_Up"))
            {
                pushing = true;
                pushDir = true;
            }
            else if (Input.GetButtonDown("P2_Down"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P2_Left"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P2_Right"))
            {
                pushing = true;
                pushDir = true;
            }
            if (Input.GetButtonUp("P2_Up"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P2_Down"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P2_Left"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P2_Right"))
            {
                pushing = false;
            }
        }
        if (CompareTag("P3"))
        {
            if (Input.GetButtonDown("P3_Up"))
            {
                pushing = true;
                pushDir = true;
            }
            else if (Input.GetButtonDown("P3_Down"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P3_Left"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P3_Right"))
            {
                pushing = true;
                pushDir = true;
            }
            if (Input.GetButtonUp("P3_Up"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P3_Down"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P3_Left"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P3_Right"))
            {
                pushing = false;
            }
        }
        if (CompareTag("P4"))
        {
            if (Input.GetButtonDown("P4_Up"))
            {
                pushing = true;
                pushDir = true;
            }
            else if (Input.GetButtonDown("P4_Down"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P4_Left"))
            {
                pushing = true;
                pushDir = false;
            }
            else if (Input.GetButtonDown("P4_Right"))
            {
                pushing = true;
                pushDir = true;
            }
            if (Input.GetButtonUp("P4_Up"))
            {
                pushing = false;
            }
            else if (Input.GetButtonUp("P4_Down"))
            {
                pushing = false;
            }
            else if (Input.GetButtonDown("P4_Left"))
            {
                pushing = false;
            }
            else if (Input.GetButtonDown("P4_Right"))
            {
                pushing = false;
            }
        }

        if (pushing)
        {
            ApplyJet(pushDir);
            smoke.transform.localRotation = Quaternion.Euler((pushDir) ? 90f : -90f, 90f, 0f);
            smoke.transform.localPosition = new Vector3(-.18f, (pushDir) ? -.165f : .25f, 0f);
            smoke.Play();
        }
        else
        {
            if (smoke != null)
                smoke.Stop();
        }

        if (CanSpin)
        {
            transform.Rotate(Time.deltaTime*currentSpeed*Vector3.forward);
        }
    }

    private void ApplyJet(bool up)
    {
        if (!up)
        {
            currentSpeed = Mathf.Min(200f, currentSpeed + adjustPower);
        }
        else
        {
            currentSpeed = Mathf.Max(-200f, currentSpeed - adjustPower);
        }
    }
}