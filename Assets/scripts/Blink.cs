﻿using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour
{
    public float blinkTime;
    public float fastBlinkTime;
    public float fastBlinkLength;
    private float currentFastTime;
    private float currentTime;
    private UILabel label;
    private bool on = true;
    private bool selected = false;
    private bool spawned = false;
    public TitleController title;
    private bool played1, played2, played3, played4;

    private void Start()
    {
        label = GetComponent<UILabel>();
    }

    private void Update()
    {
        if (CompareTag("tag_P1") && Input.GetButtonDown("P1_Button1"))
        {
            Selected();
            if (!played1)
            {
                AudioController.Play("selectthing");
                played1 = true;
            }
        }
        if (CompareTag("tag_P2") && Input.GetButtonDown("P2_Button1"))
        {
            Selected();
            if (!played2)
            {
                AudioController.Play("selectthing");
                played2 = true;
            }
        }
        if (CompareTag("tag_P3") && Input.GetButtonDown("P3_Button1"))
        {
            Selected();
            if (!played3)
            {
                AudioController.Play("selectthing");
                played3 = true;
            }
        }
        if (CompareTag("tag_P4") && Input.GetButtonDown("P4_Button1"))
        {
            Selected();
            if (!played4)
            {
                AudioController.Play("selectthing");
                played4 = true;
            }
        }

        if ((currentTime += Time.deltaTime) > blinkTime)
        {
            currentTime = 0;
            on = !on;
            if (!selected)
            {
                label.enabled = on ? true : false;
            }
            else if (selected)
            {
                if ((currentFastTime += Time.deltaTime) > fastBlinkLength)
                {
                    label.enabled = false;
                    if (!spawned)
                    {
                        spawned = true;
                        title.Selected(tag);
                    }
                }
                else
                {
                    label.enabled = on ? true : false;
                }
            }
        }
    }

    private void Selected()
    {
        blinkTime = fastBlinkTime;
        selected = true;
    }
}