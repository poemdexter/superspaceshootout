﻿using Prime31.GoKitLite;
using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public GameObject p1Prefab, p2Prefab, p3Prefab, p4Prefab;
    private GameObject player1, player2, player3, player4;
    public GameObject p1Tag, p2Tag, p3Tag, p4Tag;
    public GameObject p1Spawn, p2Spawn, p3Spawn, p4Spawn;
    public GameObject winnerBanner, buttonBanner;
    public Sprite p1f, p2f, p3f, p4f;
    public GameObject instructions;
    public float maxTime;
    public ParticleSystem blood;
    private float currentTime;
    private int playersAlive;
    private bool gameover, showTags, waitForReady, restartGame;
    private bool ready1, ready2, ready3, ready4;
    public UILabel p1s, p2s, p3s, p4s;

    private void Start()
    {
        if (instructions != null)
        {
            waitForReady = true;
        }
        else
        {
            SpawnPlayers();
            ShowTags();
        }
    }

    public void Reset()
    {
        if (player1 != null)
        {
            Destroy(player1);
        }
        if (player2 != null)
        {
            Destroy(player2);
        }
        if (player3 != null)
        {
            Destroy(player3);
        }
        if (player4 != null)
        {
            Destroy(player4);
        }
        gameover = false;
        Camera.main.transform.position = Vector3.zero + Vector3.back;
        Camera.main.orthographicSize = 5.4f;
        winnerBanner.SetActive(false);
        buttonBanner.SetActive(false);
        currentTime = 0;
        ShowTags();
        SpawnPlayers();
    }

    private void SpawnPlayers()
    {
        if (Global.Instance().p1Active)
        {
            player1 = Instantiate(p1Prefab, p1Spawn.transform.position, Quaternion.identity) as GameObject;
            if (!Global.Instance().p1male)
            {
                player1.GetComponent<SpriteRenderer>().sprite = p1f;
            }
        }
        if (Global.Instance().p2Active)
        {
            player2 = Instantiate(p2Prefab, p2Spawn.transform.position, Quaternion.identity) as GameObject;
            if (!Global.Instance().p2male)
            {
                player2.GetComponent<SpriteRenderer>().sprite = p2f;
            }
        }
        if (Global.Instance().p3Active)
        {
            player3 = Instantiate(p3Prefab, p3Spawn.transform.position, Quaternion.identity) as GameObject;
            if (!Global.Instance().p3male)
            {
                player3.GetComponent<SpriteRenderer>().sprite = p3f;
            }
        }
        if (Global.Instance().p4Active)
        {
            player4 = Instantiate(p4Prefab, p4Spawn.transform.position, Quaternion.identity) as GameObject;
            if (!Global.Instance().p4male)
            {
                player4.GetComponent<SpriteRenderer>().sprite = p4f;
            }
        }
        playersAlive = Global.Instance().GetActiveCount();
    }

    private void ShowTags()
    {
        showTags = true;

        if (Global.Instance().p1Active)
        {
            p1Tag.SetActive(true);
        }
        if (Global.Instance().p2Active)
        {
            p2Tag.SetActive(true);
        }
        if (Global.Instance().p3Active)
        {
            p3Tag.SetActive(true);
        }
        if (Global.Instance().p4Active)
        {
            p4Tag.SetActive(true);
        }
    }

    public void PlayerShot(string tag)
    {
        AudioController.Play("playerhit");
        var p = GameObject.FindGameObjectWithTag(tag);
        ParticleSystem b = (ParticleSystem) Instantiate(blood, p.transform.position, Quaternion.identity);
        b.Play();
        Destroy(p);

        switch (tag)
        {
            case "P1":
                player1 = null;
                break;
            case "P2":
                player2 = null;
                break;
            case "P3":
                player3 = null;
                break;
            case "P4":
                player4 = null;
                break;
        }

        if (--playersAlive == 1)
        {
            if (player1 != null)
            {
                player1.GetComponent<Jetpack>().CanSpin = false;
                player1.GetComponent<Shoot>().CanShoot = false;
                player1.rigidbody2D.velocity = Vector2.zero;
                DeclareWinner(player1.transform);
            }
            if (player2 != null)
            {
                player2.GetComponent<Jetpack>().CanSpin = false;
                player2.GetComponent<Shoot>().CanShoot = false;
                player2.rigidbody2D.velocity = Vector2.zero;
                DeclareWinner(player2.transform);
            }
            if (player3 != null)
            {
                player3.GetComponent<Jetpack>().CanSpin = false;
                player3.GetComponent<Shoot>().CanShoot = false;
                player3.rigidbody2D.velocity = Vector2.zero;
                DeclareWinner(player3.transform);
            }
            if (player4 != null)
            {
                player4.GetComponent<Jetpack>().CanSpin = false;
                player4.GetComponent<Shoot>().CanShoot = false;
                player4.rigidbody2D.velocity = Vector2.zero;
                DeclareWinner(player4.transform);
            }
        }
    }

    private void DeclareWinner(Transform player)
    {
        AudioController.Play("winner");
        winnerBanner.SetActive(true);

        if (player.CompareTag("P1"))
        {
            if (Global.Instance().p1Scored()) 
            {
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 1 Wins Game!";
                restartGame = true;
            }
            else
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 1 Wins Round!";
        }
        if (player.CompareTag("P2"))
        {
            if (Global.Instance().p2Scored()) 
            {
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 2 Wins Game!";
                restartGame = true;
            }
            else
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 2 Wins Round!";
        }
        if (player.CompareTag("P3"))
        {
            if (Global.Instance().p3Scored()) 
            {
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 3 Wins Game!";
                restartGame = true;
            }
            else
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 3 Wins Round!";
        }
        if (player.CompareTag("P4"))
        {
            if (Global.Instance().p4Scored())
            {
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 4 Wins Game!";
                restartGame = true;
            }
            else
                winnerBanner.transform.GetChild(0).GetComponent<UILabel>().text = "Player 4 Wins Round!";
        }

        System.Action<Transform, float> action = (trans, dt) => { trans.camera.orthographicSize = 5.2f - (3.7f*dt); };
        GoKitLite.instance.customAction(Camera.main.transform, 1f, action, 0f, GoKitLiteEasing.Cubic.EaseInOut);
        GoKitLite.instance.positionTo(Camera.main.transform, 1f, player.position - (Vector3.forward*1f), 0f,
            GoKitLiteEasing.Cubic.EaseInOut);

        p1s.text = "" + Global.Instance().GetScore1();
        p2s.text = "" + Global.Instance().GetScore2();
        p3s.text = "" + Global.Instance().GetScore3();
        p4s.text = "" + Global.Instance().GetScore4();

        StartCoroutine(WaitforEasing());
    }

    private IEnumerator WaitforEasing()
    {
        yield return new WaitForSeconds(3.0f);
        buttonBanner.SetActive(true);
        gameover = true;
    }

    private void Update()
    {
        if (Input.GetButtonDown("RESET"))
            Application.Quit();

        if (waitForReady)
        {
            if (Global.Instance().p1Active && Input.GetButtonDown("P1_Button1"))
            {
                ready1 = true;
            }
            if (Global.Instance().p2Active && Input.GetButtonDown("P2_Button1"))
            {
                ready2 = true;
            }
            if (Global.Instance().p3Active && Input.GetButtonDown("P3_Button1"))
            {
                ready3 = true;
            }
            if (Global.Instance().p4Active && Input.GetButtonDown("P4_Button1"))
            {
                ready4 = true;
            }

            int a = 0;
            if (ready1)
            {
                a++;
            }
            if (ready2)
            {
                a++;
            }
            if (ready3)
            {
                a++;
            }
            if (ready4)
            {
                a++;
            }
            if (a == Global.Instance().GetActiveCount())
            {
                waitForReady = false;
                instructions.SetActive(false);
                Reset();
            }
        }

        if (showTags && (currentTime += Time.deltaTime) > maxTime)
        {
            p1Tag.SetActive(false);
            p2Tag.SetActive(false);
            p3Tag.SetActive(false);
            p4Tag.SetActive(false);
        }

        if (gameover)
        {
            if (Input.GetButtonDown("P1_Button1") || Input.GetButtonDown("P2_Button1") ||
                Input.GetButtonDown("P3_Button1") || Input.GetButtonDown("P4_Button1"))
            {
                if (restartGame)
                    Application.LoadLevel("join");
                else
                    LoadNextLevel();
            }
        }
    }

    private void LoadNextLevel()
    {
        switch (Application.loadedLevelName)
        {
            case "tutorial":
                Application.LoadLevel("level2");
                break;
            case "level1":
                Application.LoadLevel("level2");
                break;
            case "level2":
                Application.LoadLevel("level3");
                break;
            case "level3":
                Application.LoadLevel("level4");
                break;
            case "level4":
                Application.LoadLevel("level5");
                break;
            case "level5":
                Application.LoadLevel("level6");
                break;
            case "level6":
                Application.LoadLevel("level1");
                break;
        }
    }
}