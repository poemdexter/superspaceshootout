﻿using UnityEngine;
using System.Collections;

public class PlayerScreenWrap : MonoBehaviour 
{
    public float topLimit, bottomLimit, leftLimit, rightLimit;
    public float offset;
    private Vector3 position;

    void Start()
    {
        position = transform.position;
    }

	void Update () 
    {
        if (position.y > topLimit)
        {
            position = new Vector3(position.x,bottomLimit + offset, 0);
        } 
        else if (position.y < bottomLimit) 
        {
            position = new Vector3(position.x,topLimit + offset, 0);
        } 
        else if (position.x < leftLimit)
        {
            position = new Vector3(rightLimit + offset, position.y, 0);
        }
        else if (position.x > rightLimit)
        {
            position = new Vector3(leftLimit + offset, position.y, 0);
        }
	}
}
