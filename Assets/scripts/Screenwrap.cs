﻿using UnityEngine;
using System.Collections;

public class Screenwrap : MonoBehaviour {

    public Transform partner;
    public bool isLeftRight;
    public EdgePosition edgePosition;
    public float edgeOffset;

    public enum EdgePosition
    {
        Up,
        Down,
        Left,
        Right
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("bullet"))
        {
            if (isLeftRight)
            {
                float offset = (edgePosition == EdgePosition.Left) ? -edgeOffset : edgeOffset;
                col.transform.position = new Vector3(partner.position.x + offset, col.transform.position.y, 0);
            }
            else
            {
                float offset = (edgePosition == EdgePosition.Down) ? -edgeOffset : edgeOffset;
                col.transform.position = new Vector3(col.transform.position.x, partner.position.y + offset, 0);
            }
        }
    }
}
