﻿using UnityEngine;
using System.Collections;

public class TitleController : MonoBehaviour
{
    public GameObject p1Prefab, p2Prefab, p3Prefab, p4Prefab;
    public Transform p1Spawn, p2Spawn, p3Spawn, p4Spawn;
    public Sprite p1m, p2m, p3m, p4m, p1f, p2f, p3f, p4f;
    public GameObject p1Choose, p2Choose, p3Choose, p4Choose;
    private int spawnedPlayers;
    private bool timerStarted;
    public float timerLength;
    public UILabel timerLabel;
    private bool p1SpawnOK, p2SpawnOK, p3SpawnOK, p4SpawnOK;

    private void Start()
    {
        Global.Instance().Reset();
        Screen.showCursor = false;
    }

    private void Update()
    {
        if (Input.GetButtonDown("RESET"))
            Application.Quit();

        if (spawnedPlayers > 1 && !timerStarted)
        {
            timerStarted = true;
            timerLabel.text = "" + timerLength;
            timerLabel.gameObject.SetActive(true);
        }

        CheckForGenderSwap();
    }

    public void Tick()
    {
        if (--timerLength < 0)
        {
            Application.LoadLevel("tutorial");
        }
        else
        {
            if (timerLength < 1)
            {
                timerLabel.text = "Go!";
            }
            else
            {
                timerLabel.text = "" + timerLength;
            }

            timerLabel.GetComponent<TweenScale>().ResetToBeginning();
            timerLabel.GetComponent<TweenScale>().PlayForward();
        }
    }

    public void Selected(string selectedTag)
    {
        switch (selectedTag)
        {
            case "tag_P1":
                Global.Instance().p1Active = true;
                p1Choose.SetActive(true);
                p1SpawnOK = true;
                break;
            case "tag_P2":
                Global.Instance().p2Active = true;
                p2Choose.SetActive(true);
                p2SpawnOK = true;
                break;
            case "tag_P3":
                Global.Instance().p3Active = true;
                p3Choose.SetActive(true);
                p3SpawnOK = true;
                break;
            case "tag_P4":
                Global.Instance().p4Active = true;
                p4Choose.SetActive(true);
                p4SpawnOK = true;
                break;
        }
    }

    public void SpawnPlayer(string s)
    {
        spawnedPlayers++;
        GameObject prefab = null;
        Vector3 position = Vector3.zero;
        switch (s)
        {
            case "tag_P1":
                Debug.Log("1");
                prefab = p1Prefab;
                position = p1Spawn.position;
                var go1 = (GameObject)Instantiate(prefab, position, Quaternion.identity);
                if (!Global.Instance().p1male) go1.GetComponent<SpriteRenderer>().sprite = p1f;
                break;
            case "tag_P2":
                prefab = p2Prefab;
                position = p2Spawn.position;
                var go2 = (GameObject)Instantiate(prefab, position, Quaternion.identity);
                if (!Global.Instance().p2male) go2.GetComponent<SpriteRenderer>().sprite = p2f;
                break;
            case "tag_P3":
                prefab = p3Prefab;
                position = p3Spawn.position;
                var go3 = (GameObject)Instantiate(prefab, position, Quaternion.identity);
                if (!Global.Instance().p3male) go3.GetComponent<SpriteRenderer>().sprite = p3f;
                break;
            case "tag_P4":
                prefab = p4Prefab;
                position = p4Spawn.position;
                var go4 = (GameObject)Instantiate(prefab, position, Quaternion.identity);
                if (!Global.Instance().p4male) go4.GetComponent<SpriteRenderer>().sprite = p4f;
                break;
        }
    }

    private void CheckForGenderSwap()
    {
        if (Global.Instance().p1Active && Input.GetButtonDown("P1_Button2"))
        {
            Global.Instance().p1male = true;
            if (p1SpawnOK)
            {
                AudioController.Play("selectthing");
                p1Choose.SetActive(false);
                SpawnPlayer("tag_P1");
                p1SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P1").GetComponent<SpriteRenderer>().sprite = p1m;
        }
        if (Global.Instance().p2Active && Input.GetButtonDown("P2_Button2"))
        {
            Global.Instance().p1male = true;
            if (p2SpawnOK)
            {
                AudioController.Play("selectthing");
                p2Choose.SetActive(false);
                SpawnPlayer("tag_P2");
                p2SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P2").GetComponent<SpriteRenderer>().sprite = p2m;
        }
        if (Global.Instance().p3Active && Input.GetButtonDown("P3_Button2"))
        {
            Global.Instance().p3male = true;
            if (p3SpawnOK)
            {
                AudioController.Play("selectthing");
                p3Choose.SetActive(false);
                SpawnPlayer("tag_P3");
                p3SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P3").GetComponent<SpriteRenderer>().sprite = p3m;
        }
        if (Global.Instance().p4Active && Input.GetButtonDown("P4_Button2"))
        {
            Global.Instance().p4male = true;
            if (p4SpawnOK)
            {
                AudioController.Play("selectthing");
                p4Choose.SetActive(false);
                SpawnPlayer("tag_P4");
                p4SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P4").GetComponent<SpriteRenderer>().sprite = p4m;
        }
        if (Global.Instance().p1Active && Input.GetButtonDown("P1_Button3"))
        {
            Global.Instance().p1male = false;
            if (p1SpawnOK)
            {
                AudioController.Play("selectthing");
                p1Choose.SetActive(false);
                SpawnPlayer("tag_P1");
                p1SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P1").GetComponent<SpriteRenderer>().sprite = p1f;
        }
        if (Global.Instance().p2Active && Input.GetButtonDown("P2_Button3"))
        {
            Global.Instance().p1male = false;
            if (p2SpawnOK)
            {
                AudioController.Play("selectthing");
                p2Choose.SetActive(false);
                SpawnPlayer("tag_P2");
                p2SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P2").GetComponent<SpriteRenderer>().sprite = p2f;
        }
        if (Global.Instance().p3Active && Input.GetButtonDown("P3_Button3"))
        {
            Global.Instance().p3male = false;
            if (p3SpawnOK)
            {
                AudioController.Play("selectthing");
                p3Choose.SetActive(false);
                SpawnPlayer("tag_P3");
                p3SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P3").GetComponent<SpriteRenderer>().sprite = p3f;
        }
        if (Global.Instance().p4Active && Input.GetButtonDown("P4_Button3"))
        {
            Global.Instance().p4male = false;
            if (p4SpawnOK)
            {
                AudioController.Play("selectthing");
                p4Choose.SetActive(false);
                SpawnPlayer("tag_P4");
                p4SpawnOK = false;
            }
            GameObject.FindGameObjectWithTag("P4").GetComponent<SpriteRenderer>().sprite = p4f;
        }
    }
}