﻿using System.Runtime.Serialization.Formatters;
using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public Vector2 fireOffset;
    public float kickback;
    public bool CanShoot { get; set; }

    public void Start()
    {
        CanShoot = true;
    }

    private void Update()
    {
        if (CompareTag("P1") && Input.GetButtonDown("P1_Button1"))
        {
            Fire();
        }
        if (CompareTag("P2") && Input.GetButtonDown("P2_Button1"))
        {
            Fire();
        }
        if (CompareTag("P3") && Input.GetButtonDown("P3_Button1"))
        {
            Fire();
        }
        if (CompareTag("P4") && Input.GetButtonDown("P4_Button1"))
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (CanShoot)
        {
            AudioController.Play("shootgun");
            Vector3 right = (transform.right*fireOffset.x);
            Vector3 up = (transform.up*fireOffset.y);
            var b = (GameObject) Instantiate(bullet, transform.position + right + up, transform.rotation);
            rigidbody2D.AddForce(-transform.right*kickback);
            b.GetComponent<Owner>().Owns = tag;
        }
    }
}