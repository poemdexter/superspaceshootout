﻿using UnityEngine;
using System.Collections;

public class SpawnAudio : MonoBehaviour {

    public GameObject audioPrefab;

	void Start () {
	    if (GameObject.FindGameObjectWithTag("AudioController") == null)
        {
            Instantiate(audioPrefab);
        }
	}
}
