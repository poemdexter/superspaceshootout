﻿using UnityEngine;
using System.Collections;

public class Fired : MonoBehaviour
{
    public float speed;

    private void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("barrier") || c.CompareTag("electric"))
        {
            Destroy(gameObject);
        }
    }
}