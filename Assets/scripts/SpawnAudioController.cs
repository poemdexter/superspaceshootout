﻿using UnityEngine;
using System.Collections;

public class SpawnAudioController : MonoBehaviour
{
    public GameObject audioGameObject;

    private void Start()
    {
        var a = GameObject.FindGameObjectWithTag("AudioController");
        if (a == null)
            Instantiate(audioGameObject);
    }
}
