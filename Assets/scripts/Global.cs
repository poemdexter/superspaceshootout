﻿public class Global
{
    public bool p1Active, p2Active, p3Active, p4Active;
    public bool p1male = true;
    public bool p2male = true;
    public bool p3male = true;
    public bool p4male = true;

    private int p1Score, p2Score, p3Score, p4Score;

    private static Global instance;

    public static Global Instance()
    {
        if (instance == null)
        {
            instance = new Global();
        }
        return instance;
    }

    public void Reset()
    {
        p1Active = false;
        p2Active = false;
        p3Active = false;
        p4Active = false;
        p1Score = 0;
        p2Score = 0;
        p3Score = 0;
        p4Score = 0;
    }

    public int GetScore1()
    {
        return p1Score;
    }

    public int GetScore2()
    {
        return p2Score;
    }

    public int GetScore3()
    {
        return p3Score;
    }

    public int GetScore4()
    {
        return p4Score;
    }

    public bool p1Scored()
    {
        p1Score++;
        return p1Score == 3;
    }

    public bool p2Scored()
    {
        p2Score++;
        return p2Score == 3;
    }

    public bool p3Scored()
    {
        p3Score++;
        return p3Score == 3;
    }

    public bool p4Scored()
    {
        p4Score++;
        return p4Score == 3;
    }

    public int GetActiveCount()
    {
        int num = 0;
        num = (p1Active) ? ++num : num;
        num = (p2Active) ? ++num : num;
        num = (p3Active) ? ++num : num;
        num = (p4Active) ? ++num : num;
        return num;
    }
}