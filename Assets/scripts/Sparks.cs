﻿using UnityEngine;
using System.Collections;

public class Sparks : MonoBehaviour
{
    public ParticleSystem sparks;
 
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("station"))
        {
            var p = (ParticleSystem) Instantiate(sparks, transform.position, transform.rotation);
            p.Play();
            Destroy(gameObject);
        }
    }
}
