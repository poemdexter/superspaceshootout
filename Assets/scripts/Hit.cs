﻿using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour
{
    private GameController game;

    private void Start()
    {
        var g = GameObject.FindGameObjectWithTag("GameController");
        if (g != null)
            game = g.GetComponent<GameController>();
    }

    private void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.CompareTag("bullet") && c.gameObject.GetComponent<Owner>().Owns != tag)
        {
            Destroy(c.gameObject);
            game.PlayerShot(tag);
        }
        else if (c.gameObject.CompareTag("electric"))
        {
            game.PlayerShot(tag);
        }
    }
}